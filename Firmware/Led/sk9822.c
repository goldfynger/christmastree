#include "sk9822_hal.h"
#include "sk9822.h"


const SK9822_RgbColorTypeDef _RgbColors[SK9822_RGB_INDEX_COUNT] =
{
    SK9822_RGB_RED,
    SK9822_RGB_GREEN,
    SK9822_RGB_BLUE,
};

const SK9822_RainbowColorTypeDef _RainbowColors[SK9822_RAINBOW_INDEX_COUNT] =
{
    SK9822_RAINBOW_RED,
    SK9822_RAINBOW_YELLOW,
    SK9822_RAINBOW_GREEN,
    SK9822_RAINBOW_CYAN,
    SK9822_RAINBOW_BLUE,
    SK9822_RAINBOW_MAGENTA,
};


SK9822_ErrorTypeDef SK9822_WriteData(SK9822_FrameTypeDef * pFrameArray, uint16_t length)
{
    return SK9822_HAL_TransmitSync((uint8_t *)pFrameArray, length * 4);
}

void SK9822_SetColorRgb(SK9822_FrameTypeDef * pFrame, SK9822_RgbColorTypeDef color)
{
    pFrame->Raw &= ~SK9822_RGB_MASK;
    pFrame->Raw |= color;
}

void SK9822_SetColorRainbow(SK9822_FrameTypeDef * pFrame, SK9822_RainbowColorTypeDef color)
{
    pFrame->Raw &= ~SK9822_RAINBOW_MASK;
    pFrame->Raw |= color;
}

void SK9822_SetColorByIndexRgb(SK9822_FrameTypeDef * pFrame, SK9822_RgbColorIndexTypeDef colorIndex)
{
    pFrame->Raw &= ~SK9822_RGB_MASK;
    pFrame->Raw |= _RgbColors[colorIndex];
}

void SK9822_SetColorByIndexRainbow(SK9822_FrameTypeDef * pFrame, SK9822_RainbowColorIndexTypeDef colorIndex)
{
    pFrame->Raw &= ~SK9822_RAINBOW_MASK;
    pFrame->Raw |= _RainbowColors[colorIndex];
}

void SK9822_SetBrightness(SK9822_FrameTypeDef * pFrame, uint8_t brightness)
{
    if (brightness > SK9822_SPECIAL_MAX_BRIGHTNESS)
    {
        brightness = SK9822_SPECIAL_MAX_BRIGHTNESS;
    }
    
    pFrame->ReservedBrightness = SK9822_SPECIAL_RESERVED_BRIGHTNESS;
    pFrame->Brightness = brightness;
}

void SK9822_NextColorRgb(SK9822_FrameTypeDef * pFrame)
{
    if (pFrame->Red == SK9822_SPECIAL_MAX_COLOR)
    {
        SK9822_SetColorRgb(pFrame, SK9822_RGB_GREEN);
    }
    else if (pFrame->Green == SK9822_SPECIAL_MAX_COLOR)
    {
        SK9822_SetColorRgb(pFrame, SK9822_RGB_BLUE);
    }
    else if (pFrame->Blue == SK9822_SPECIAL_MAX_COLOR)
    {
        SK9822_SetColorRgb(pFrame, SK9822_RGB_RED);
    }
}

void SK9822_PreviousColorRgb(SK9822_FrameTypeDef * pFrame)
{
    if (pFrame->Red == SK9822_SPECIAL_MAX_COLOR)
    {
        SK9822_SetColorRgb(pFrame, SK9822_RGB_BLUE);
    }
    else if (pFrame->Blue == SK9822_SPECIAL_MAX_COLOR)
    {
        SK9822_SetColorRgb(pFrame, SK9822_RGB_GREEN);
    }
    else if (pFrame->Green == SK9822_SPECIAL_MAX_COLOR)
    {
        SK9822_SetColorRgb(pFrame, SK9822_RGB_RED);
    }
}

void SK9822_NextColorRainbow(SK9822_FrameTypeDef * pFrame)
{
    /* Red OR in way to green. */
    if ((pFrame->Red == SK9822_SPECIAL_MAX_COLOR) ||
        (pFrame->Red != SK9822_SPECIAL_MIN_COLOR && pFrame->Green != SK9822_SPECIAL_MIN_COLOR))
    {
        pFrame->Red -= 1;
        pFrame->Green += 1;
    }
    /* Green OR in way to blue. */
    else if ((pFrame->Green == SK9822_SPECIAL_MAX_COLOR) ||
             (pFrame->Green != SK9822_SPECIAL_MIN_COLOR && pFrame->Blue != SK9822_SPECIAL_MIN_COLOR))
    {
        pFrame->Green -= 1;
        pFrame->Blue += 1;
    }
    /* Blue OR in way to red. */
    else if ((pFrame->Blue == SK9822_SPECIAL_MAX_COLOR) ||
             (pFrame->Blue != SK9822_SPECIAL_MIN_COLOR && pFrame->Red != SK9822_SPECIAL_MIN_COLOR))
    {
        pFrame->Blue -= 1;
        pFrame->Red += 1;
    }
}

void SK9822_PreviousColorRainbow(SK9822_FrameTypeDef * pFrame)
{
    /* Red OR in way to blue. */
    if ((pFrame->Red == SK9822_SPECIAL_MAX_COLOR) ||
        (pFrame->Red != SK9822_SPECIAL_MIN_COLOR && pFrame->Blue != SK9822_SPECIAL_MIN_COLOR))
    {
        pFrame->Red -= 1;
        pFrame->Blue += 1;
    }
    /* Blue OR in way to green. */
    else if ((pFrame->Blue == SK9822_SPECIAL_MAX_COLOR) ||
             (pFrame->Blue != SK9822_SPECIAL_MIN_COLOR && pFrame->Green != SK9822_SPECIAL_MIN_COLOR))
    {
        pFrame->Blue -= 1;
        pFrame->Green += 1;
    }
    /* Green OR in way to red. */
    else if ((pFrame->Green == SK9822_SPECIAL_MAX_COLOR) ||
             (pFrame->Green != SK9822_SPECIAL_MIN_COLOR && pFrame->Red != SK9822_SPECIAL_MIN_COLOR))
    {
        pFrame->Green -= 1;
        pFrame->Red += 1;
    }
}

void SK9822_IncreaseBrightness(SK9822_FrameTypeDef * pFrame)
{
    if (pFrame->Brightness < SK9822_SPECIAL_MAX_BRIGHTNESS)
    {
        pFrame->Brightness += 1;
    }
}

void SK9822_ReduceBrightness(SK9822_FrameTypeDef * pFrame)
{
    if (pFrame->Brightness > SK9822_SPECIAL_MIN_BRIGHTNESS)
    {
        pFrame->Brightness -= 1;
    }
}

void SK9822_AddStartEndFrames(SK9822_FrameTypeDef * pFrameArray, uint16_t length)
{
    if (length >= 2)
    {
        pFrameArray[0].Raw = SK9822_SPECIAL_START_FRAME;
        pFrameArray[length - 1].Raw = SK9822_SPECIAL_END_FRAME;
    }
}
