#ifndef __SK9822_HAL_H
#define __SK9822_HAL_H


#include <stdint.h>
#include "sk9822.h"


SK9822_ErrorTypeDef SK9822_HAL_TransmitSync(uint8_t *pTxData, uint16_t length);


#endif /* __SK9822_HAL_H */
