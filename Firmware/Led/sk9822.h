#ifndef __SK9822_H
#define __SK9822_H


#include <stdint.h>


#define SK9822_SPECIAL_START_FRAME              ((uint32_t)0x00000000)
#define SK9822_SPECIAL_END_FRAME                ((uint32_t)0xFFFFFFFF)

#define SK9822_SPECIAL_RESERVED_BRIGHTNESS      ((uint8_t)0x07)
#define SK9822_SPECIAL_MIN_BRIGHTNESS           ((uint8_t)0x00)
#define SK9822_SPECIAL_MAX_BRIGHTNESS           ((uint8_t)0x1F)

#define SK9822_SPECIAL_RESERVED_BRIGHTNESS_SIZE ((uint8_t)3)
#define SK9822_SPECIAL_BRIGHTNESS_SIZE          ((uint8_t)5)

#define SK9822_SPECIAL_MIN_COLOR                ((uint8_t)0x00)
#define SK9822_SPECIAL_MAX_COLOR                ((uint8_t)0xFF)


typedef enum
{
    SK9822_ERR_OK               = 0,
    SK9822_ERR_NULLPTR          = 1,
    
    SK9822_ERR_HAL              = 32,
    SK9822_ERR_NOT_IMPLIMENTED  = 33,
}
SK9822_ErrorTypeDef;

typedef enum
{
    SK9822_RGB_NONE         = 0x00000000,
    
    SK9822_RGB_RED          = 0x000000FF,   
    SK9822_RGB_GREEN        = 0x0000FF00,
    SK9822_RGB_BLUE         = 0x00FF0000,
    
    SK9822_RGB_WHITE        = 0x00FFFFFF,
    SK9822_RGB_MASK         = 0x00FFFFFF,
}
SK9822_RgbColorTypeDef;

typedef enum
{
    SK9822_RAINBOW_NONE     = 0x00000000,
    
    SK9822_RAINBOW_RED      = 0x000000FF,
    SK9822_RAINBOW_YELLOW   = 0x00007F80,
    SK9822_RAINBOW_GREEN    = 0x0000FF00,
    SK9822_RAINBOW_CYAN     = 0x007F8000,
    SK9822_RAINBOW_BLUE     = 0x00FF0000,
    SK9822_RAINBOW_MAGENTA  = 0x0080007F,
    
    SK9822_RAINBOW_WHITE    = 0x00FFFFFF,
    SK9822_RAINBOW_MASK     = 0x00FFFFFF,
}
SK9822_RainbowColorTypeDef;

typedef enum
{
    SK9822_RGB_INDEX_RED    = 0,
    SK9822_RGB_INDEX_GREEN  = 1,
    SK9822_RGB_INDEX_BLUE   = 2,
    
    SK9822_RGB_INDEX_COUNT  = 3,
}
SK9822_RgbColorIndexTypeDef;

typedef enum
{    
    SK9822_RAINBOW_INDEX_RED        = 0,
    SK9822_RAINBOW_INDEX_YELLOW     = 1,
    SK9822_RAINBOW_INDEX_GREEN      = 2,
    SK9822_RAINBOW_INDEX_CYAN       = 3,
    SK9822_RAINBOW_INDEX_BLUE       = 4,
    SK9822_RAINBOW_INDEX_MAGENTA    = 5,
    
    SK9822_RAINBOW_INDEX_COUNT      = 6,
}
SK9822_RainbowColorIndexTypeDef;


#pragma anon_unions

typedef union
{
    uint32_t    Raw;
    
    struct
    {
        uint8_t ReservedBrightness : SK9822_SPECIAL_RESERVED_BRIGHTNESS_SIZE;
        uint8_t Brightness : SK9822_SPECIAL_BRIGHTNESS_SIZE;  
        
        uint8_t Blue;
        uint8_t Green;
        uint8_t Red;
    };
}
SK9822_FrameTypeDef;

#pragma no_anon_unions


SK9822_ErrorTypeDef SK9822_WriteData(SK9822_FrameTypeDef * pFrameArray, uint16_t length);

void SK9822_SetColorRgb(SK9822_FrameTypeDef * pFrame, SK9822_RgbColorTypeDef color);
void SK9822_SetColorRainbow(SK9822_FrameTypeDef * pFrame, SK9822_RainbowColorTypeDef color);
void SK9822_SetColorByIndexRgb(SK9822_FrameTypeDef * pFrame, SK9822_RgbColorIndexTypeDef colorIndex);
void SK9822_SetColorByIndexRainbow(SK9822_FrameTypeDef * pFrame, SK9822_RainbowColorIndexTypeDef colorIndex);
void SK9822_SetBrightness(SK9822_FrameTypeDef * pFrame, uint8_t brightness);
void SK9822_NextColorRgb(SK9822_FrameTypeDef * pFrame);
void SK9822_PreviousColorRgb(SK9822_FrameTypeDef * pFrame);
void SK9822_NextColorRainbow(SK9822_FrameTypeDef * pFrame);
void SK9822_PreviousColorRainbow(SK9822_FrameTypeDef * pFrame);
void SK9822_IncreaseBrightness(SK9822_FrameTypeDef * pFrame);
void SK9822_ReduceBrightness(SK9822_FrameTypeDef * pFrame);
void SK9822_AddStartEndFrames(SK9822_FrameTypeDef * pFrameArray, uint16_t length);


#endif /* __SK9822_H */
