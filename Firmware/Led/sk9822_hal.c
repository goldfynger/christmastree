#include "sk9822.h"
#include "sk9822_hal.h"


#ifndef UNUSED
#define UNUSED(X) (void)X
#endif


__weak SK9822_ErrorTypeDef SK9822_HAL_TransmitSync(uint8_t *pTxData, uint16_t length)
{
    UNUSED(pTxData);
    UNUSED(length);
    
    return SK9822_ERR_NOT_IMPLIMENTED;
}
