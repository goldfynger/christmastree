#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include "sk9822.h"
#include "stm32f1xx_hal.h"
#include "main.h"
#include "rgb.h"


#define RGB_FRAME_ARRAY_LENGTH  ((uint16_t)(1 + LEDTESTER_LED_COUNT + 1))


static SK9822_FrameTypeDef _frameArray[RGB_FRAME_ARRAY_LENGTH];

static bool _useBrightness;
static bool _countUp;

static uint32_t _duration;
static uint32_t _startTimeout;

static uint8_t _firstLedColorIndex;

static bool _started = false;


static void RGB_StartInternal(uint32_t duration);
static void RGB_UpdateLeds(void);


void RGB_Start(uint32_t msSwitchStepDuration)
{
    _useBrightness = false;
    
    _countUp = false;
    
    RGB_StartInternal(msSwitchStepDuration);
}

void RGB_StartWithBrightness(uint32_t msBrightnessStepDuration)
{
    _useBrightness = true;
    
    _countUp = true;
    
    RGB_StartInternal(msBrightnessStepDuration);
}

void RGB_Process(void)
{
    if (!_started)
    {
        assert_failed((uint8_t *)__FILE__, __LINE__);
    }
    
    if ((HAL_GetTick() - _startTimeout) < _duration)
    {
        return;
    }
    
    
    if (_useBrightness)
    {
        if (!_countUp && _frameArray[1].Brightness == SK9822_SPECIAL_MIN_BRIGHTNESS)
        {
            _firstLedColorIndex = (_firstLedColorIndex + 1) % SK9822_RGB_INDEX_COUNT;
        }
        
        for (uint8_t i = 1; i < RGB_FRAME_ARRAY_LENGTH - 1; i++)
        {
            SK9822_FrameTypeDef *pFrame = &_frameArray[i];
            
            uint8_t currentBrightness = pFrame->Brightness;
            
            if (_countUp)
            {
                if (currentBrightness == SK9822_SPECIAL_MAX_BRIGHTNESS)
                {
                    _countUp = false;
                    
                    SK9822_ReduceBrightness(pFrame);
                }
                else
                {
                    SK9822_IncreaseBrightness(pFrame);
                }
            }
            else
            {
                if (currentBrightness == SK9822_SPECIAL_MIN_BRIGHTNESS)
                {
                    _countUp = true;
                    
                    uint8_t colorIndex = (i - 1 + _firstLedColorIndex) % SK9822_RGB_INDEX_COUNT;
                    
                    SK9822_SetColorByIndexRgb(pFrame, (SK9822_RgbColorIndexTypeDef)colorIndex);
                }
                else
                {
                    SK9822_ReduceBrightness(pFrame);
                }
            }
        }
    }
    else
    {
        _firstLedColorIndex = (_firstLedColorIndex + 1) % SK9822_RGB_INDEX_COUNT;
        
        for (uint8_t i = 1; i < RGB_FRAME_ARRAY_LENGTH - 1; i++)
        {
            SK9822_FrameTypeDef *pFrame = &_frameArray[i];
            
            uint8_t colorIndex = (i - 1 + _firstLedColorIndex) % SK9822_RGB_INDEX_COUNT;
            
            SK9822_SetColorByIndexRgb(pFrame, (SK9822_RgbColorIndexTypeDef)colorIndex);
        }
    }
    
    _startTimeout = HAL_GetTick();
    
    RGB_UpdateLeds();
}

void RGB_Stop(void)
{
    if (!_started)
    {
        assert_failed((uint8_t *)__FILE__, __LINE__);
    }
    
    _started = false;
    
    
    for (uint8_t i = 1; i < RGB_FRAME_ARRAY_LENGTH - 1; i++)
    {
        SK9822_FrameTypeDef *pFrame = &_frameArray[i];
        
        SK9822_SetBrightness(pFrame, SK9822_SPECIAL_MIN_BRIGHTNESS);
        
        SK9822_SetColorRgb(pFrame, SK9822_RGB_NONE);
    }
    
    RGB_UpdateLeds();
}

static void RGB_StartInternal(uint32_t duration)
{
    if (_started)
    {
        assert_failed((uint8_t *)__FILE__, __LINE__);
    }
    
    _started = true;
    
    
    if (RGB_FRAME_ARRAY_LENGTH <= 2)
    {
        assert_failed((uint8_t *)__FILE__, __LINE__);
    }
    
    
    SK9822_AddStartEndFrames(_frameArray, RGB_FRAME_ARRAY_LENGTH);
    
    
    uint8_t startBrightness = _useBrightness ? SK9822_SPECIAL_MIN_BRIGHTNESS : SK9822_SPECIAL_MAX_BRIGHTNESS;
    
    _firstLedColorIndex = SK9822_RGB_INDEX_RED;
    
    for (uint8_t i = 1; i < RGB_FRAME_ARRAY_LENGTH - 1; i++)
    {
        SK9822_FrameTypeDef *pFrame = &_frameArray[i];
        
        SK9822_SetBrightness(pFrame, startBrightness);
        
        SK9822_SetColorRgb(pFrame, SK9822_RGB_NONE);
        
        
        uint8_t colorIndex = (i - 1 + _firstLedColorIndex) % SK9822_RGB_INDEX_COUNT;
        
        SK9822_SetColorByIndexRgb(pFrame, (SK9822_RgbColorIndexTypeDef)colorIndex);
    }
    
    _duration = duration;
    _startTimeout = HAL_GetTick();
    
    RGB_UpdateLeds();
}

static void RGB_UpdateLeds(void)
{
    if (SK9822_WriteData(_frameArray, RGB_FRAME_ARRAY_LENGTH) != SK9822_ERR_OK)
    {
        assert_failed((uint8_t *)__FILE__, __LINE__);
    }
}
