#ifndef __RAINBOW_H
#define __RAINBOW_H


#include <stdint.h>


void RAINBOW_Start(uint32_t msSwitchColorDuration);
void RAINBOW_Process(void);
void RAINBOW_Stop(void);


#endif /* __RAINBOW_H */
