#include <stdbool.h>
#include <stdint.h>
#include "sk9822.h"
#include "main.h"
#include "rainbow.h"


#define RAINBOW_FRAME_ARRAY_LENGTH  ((uint16_t)(1 + LEDTESTER_LED_COUNT + 1))


static SK9822_FrameTypeDef _frameArray[RAINBOW_FRAME_ARRAY_LENGTH];

static uint32_t _duration;
static uint32_t _startTimeout;

static bool _started = false;


static void RAINBOW_UpdateLeds(void);


void RAINBOW_Start(uint32_t msSwitchColorDuration)
{
    if (_started)
    {
        assert_failed((uint8_t *)__FILE__, __LINE__);
    }
    
    _started = true;
    
    
    if (RAINBOW_FRAME_ARRAY_LENGTH <= 2)
    {
        assert_failed((uint8_t *)__FILE__, __LINE__);
    }
    
    
    SK9822_AddStartEndFrames(_frameArray, RAINBOW_FRAME_ARRAY_LENGTH);
    
    
    for (uint8_t i = 1; i < RAINBOW_FRAME_ARRAY_LENGTH - 1; i++)
    {
        SK9822_FrameTypeDef *pFrame = &_frameArray[i];
        
        SK9822_SetBrightness(pFrame, SK9822_SPECIAL_MAX_BRIGHTNESS);
        
        uint8_t colorIndex = (i - 1) % SK9822_RAINBOW_INDEX_COUNT;
        
        SK9822_SetColorByIndexRainbow(pFrame, (SK9822_RainbowColorIndexTypeDef)colorIndex);
    }
    
    _duration = msSwitchColorDuration;
    _startTimeout = HAL_GetTick();
    
    RAINBOW_UpdateLeds();
}

void RAINBOW_Process(void)
{
    if (!_started)
    {
        assert_failed((uint8_t *)__FILE__, __LINE__);
    }    
    
    if ((HAL_GetTick() - _startTimeout) < _duration)
    {
        return;
    }
    
    
    for (uint8_t i = 1; i < RAINBOW_FRAME_ARRAY_LENGTH - 1; i++)
    {
        SK9822_FrameTypeDef *pFrame = &_frameArray[i];
        
        SK9822_NextColorRainbow(pFrame);
    }
    
    _startTimeout = HAL_GetTick();
    
    RAINBOW_UpdateLeds();    
}

void RAINBOW_Stop(void)
{
    if (!_started)
    {
        assert_failed((uint8_t *)__FILE__, __LINE__);
    }
    
    _started = false;
    
    
    for (uint8_t i = 1; i < RAINBOW_FRAME_ARRAY_LENGTH - 1; i++)
    {
        SK9822_FrameTypeDef *pFrame = &_frameArray[i];
        
        SK9822_SetBrightness(pFrame, SK9822_SPECIAL_MIN_BRIGHTNESS);
        
        SK9822_SetColorRainbow(pFrame, SK9822_RAINBOW_NONE);
    }
    
    RAINBOW_UpdateLeds();
}

static void RAINBOW_UpdateLeds(void)
{
    if (SK9822_WriteData(_frameArray, RAINBOW_FRAME_ARRAY_LENGTH) != SK9822_ERR_OK)
    {
        assert_failed((uint8_t *)__FILE__, __LINE__);
    }
}
