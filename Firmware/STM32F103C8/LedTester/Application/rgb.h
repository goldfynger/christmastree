#ifndef __RGB_H
#define __RGB_H


#include <stdint.h>


void RGB_Start(uint32_t msSwitchStepDuration);
void RGB_StartWithBrightness(uint32_t msBrightnessStepDuration);
void RGB_Process(void);
void RGB_Stop(void);


#endif /* __RGB_H */
