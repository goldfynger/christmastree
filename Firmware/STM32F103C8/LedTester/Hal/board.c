#include <stdbool.h>
#include <stdint.h>
#include "stm32f1xx_hal.h"
#include "sk9822.h"
#include "sk9822_hal.h"
#include "main.h"
#include "board.h"


extern SPI_HandleTypeDef hspi1;


/* Sets SPI SCK and MOSI in work (low) state before exchange. */
void BOARD_SPI_IdleTransmit(void)
{
    static uint8_t ignored = 0x00;
    
    if (HAL_SPI_TransmitReceive(&hspi1, &ignored, &ignored, 1, HAL_MAX_DELAY) != HAL_OK)
    {
        Error_Handler();
    }
}

/* Weak callback. */
SK9822_ErrorTypeDef SK9822_HAL_TransmitSync(uint8_t *pTxData, uint16_t length)
{    
    if (HAL_SPI_Transmit(&hspi1, pTxData, length, HAL_MAX_DELAY) != HAL_OK)
    {
        return SK9822_ERR_HAL;
    }
    
    return SK9822_ERR_OK;
}
