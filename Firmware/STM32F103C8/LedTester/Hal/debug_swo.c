#include <stdio.h>
#include "debug_swo.h"


FILE __stdout;
FILE __stdin;


int fputc(int ch, FILE *f)
{
    if (DEMCR & TRCENA)
    {
        while (ITM_Port32(0) == 0) ;
        
        ITM_Port8(0) = ch;
    }
    return(ch);
}
