#ifndef __BOARD_H
#define __BOARD_H


#include <stdbool.h>


bool BOARD_IsVUsbOn             (void);
bool BOARD_IsVBatOn             (void);
bool BOARD_IsNrf24IrqOn         (void);

void BOARD_SetUsbPullUpState    (bool pullUpState);
void BOARD_SetVBatLedState      (bool vBatLedState);
void BOARD_InvertVBatLedState   (void);
void BOARD_SetTxLedState        (bool txLedState);
void BOARD_SetRxLedState        (bool rxLedState);
void BOARD_SetNrf24NssState     (bool nrf24NssState);
void BOARD_SetNrf24CeState      (bool nrf24CeState);

void BOARD_SPI_IdleTransmit     (void);

void BOARD_VUsbStateChanged     (bool vUsbState);
void BOARD_VBatStateChanged     (bool vBatState);
void BOARD_Nrf24IrqStateChanged (bool nrf24IrqState);


#endif /* __BOARD_H */
