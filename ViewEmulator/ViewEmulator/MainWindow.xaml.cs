﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Animation;

namespace ViewEmulator
{
    public partial class MainWindow : Window
    {
        private static readonly DependencyProperty Led1BrushProperty = DependencyProperty.Register(nameof(Led1Brush), typeof(Brush), typeof(MainWindow), new PropertyMetadata(Brushes.White));
        private static readonly DependencyProperty Led2BrushProperty = DependencyProperty.Register(nameof(Led2Brush), typeof(Brush), typeof(MainWindow), new PropertyMetadata(Brushes.White));
        private static readonly DependencyProperty Led3BrushProperty = DependencyProperty.Register(nameof(Led3Brush), typeof(Brush), typeof(MainWindow), new PropertyMetadata(Brushes.White));
        private static readonly DependencyProperty Led4BrushProperty = DependencyProperty.Register(nameof(Led4Brush), typeof(Brush), typeof(MainWindow), new PropertyMetadata(Brushes.White));
        private static readonly DependencyProperty Led5BrushProperty = DependencyProperty.Register(nameof(Led5Brush), typeof(Brush), typeof(MainWindow), new PropertyMetadata(Brushes.White));
        private static readonly DependencyProperty Led6BrushProperty = DependencyProperty.Register(nameof(Led6Brush), typeof(Brush), typeof(MainWindow), new PropertyMetadata(Brushes.White));
        private static readonly DependencyProperty Led7BrushProperty = DependencyProperty.Register(nameof(Led7Brush), typeof(Brush), typeof(MainWindow), new PropertyMetadata(Brushes.White));
        private static readonly DependencyProperty Led8BrushProperty = DependencyProperty.Register(nameof(Led8Brush), typeof(Brush), typeof(MainWindow), new PropertyMetadata(Brushes.White));
        private static readonly DependencyProperty Led9BrushProperty = DependencyProperty.Register(nameof(Led9Brush), typeof(Brush), typeof(MainWindow), new PropertyMetadata(Brushes.White));
        private static readonly DependencyProperty Led10BrushProperty = DependencyProperty.Register(nameof(Led10Brush), typeof(Brush), typeof(MainWindow), new PropertyMetadata(Brushes.White));
        private static readonly DependencyProperty Led11BrushProperty = DependencyProperty.Register(nameof(Led11Brush), typeof(Brush), typeof(MainWindow), new PropertyMetadata(Brushes.White));
        private static readonly DependencyProperty Led12BrushProperty = DependencyProperty.Register(nameof(Led12Brush), typeof(Brush), typeof(MainWindow), new PropertyMetadata(Brushes.White));
        private static readonly DependencyProperty Led13BrushProperty = DependencyProperty.Register(nameof(Led13Brush), typeof(Brush), typeof(MainWindow), new PropertyMetadata(Brushes.White));
        private static readonly DependencyProperty Led14BrushProperty = DependencyProperty.Register(nameof(Led14Brush), typeof(Brush), typeof(MainWindow), new PropertyMetadata(Brushes.White));
        private static readonly DependencyProperty Led15BrushProperty = DependencyProperty.Register(nameof(Led15Brush), typeof(Brush), typeof(MainWindow), new PropertyMetadata(Brushes.White));


        public MainWindow()
        {
            InitializeComponent();

            bStart.Click += BStart_Click;
        }


        private void BStart_Click(object sender, RoutedEventArgs e)
        {
            var rainbowAnimationSet = new RainbowAnimationSet(this);

            rainbowAnimationSet.Run();
        }


        public Brush Led1Brush
        {
            get { return (Brush)GetValue(Led1BrushProperty); }
            set { SetValue(Led1BrushProperty, value); }
        }
        public Brush Led2Brush
        {
            get { return (Brush)GetValue(Led2BrushProperty); }
            set { SetValue(Led2BrushProperty, value); }
        }
        public Brush Led3Brush
        {
            get { return (Brush)GetValue(Led3BrushProperty); }
            set { SetValue(Led3BrushProperty, value); }
        }

        public Brush Led4Brush
        {
            get { return (Brush)GetValue(Led4BrushProperty); }
            set { SetValue(Led4BrushProperty, value); }
        }

        public Brush Led5Brush
        {
            get { return (Brush)GetValue(Led5BrushProperty); }
            set { SetValue(Led5BrushProperty, value); }
        }

        public Brush Led6Brush
        {
            get { return (Brush)GetValue(Led6BrushProperty); }
            set { SetValue(Led6BrushProperty, value); }
        }

        public Brush Led7Brush
        {
            get { return (Brush)GetValue(Led7BrushProperty); }
            set { SetValue(Led7BrushProperty, value); }
        }

        public Brush Led8Brush
        {
            get { return (Brush)GetValue(Led8BrushProperty); }
            set { SetValue(Led8BrushProperty, value); }
        }

        public Brush Led9Brush
        {
            get { return (Brush)GetValue(Led9BrushProperty); }
            set { SetValue(Led9BrushProperty, value); }
        }

        public Brush Led10Brush
        {
            get { return (Brush)GetValue(Led10BrushProperty); }
            set { SetValue(Led10BrushProperty, value); }
        }

        public Brush Led11Brush
        {
            get { return (Brush)GetValue(Led11BrushProperty); }
            set { SetValue(Led11BrushProperty, value); }
        }

        public Brush Led12Brush
        {
            get { return (Brush)GetValue(Led12BrushProperty); }
            set { SetValue(Led12BrushProperty, value); }
        }

        public Brush Led13Brush
        {
            get { return (Brush)GetValue(Led13BrushProperty); }
            set { SetValue(Led13BrushProperty, value); }
        }

        public Brush Led14Brush
        {
            get { return (Brush)GetValue(Led14BrushProperty); }
            set { SetValue(Led14BrushProperty, value); }
        }

        public Brush Led15Brush
        {
            get { return (Brush)GetValue(Led15BrushProperty); }
            set { SetValue(Led15BrushProperty, value); }
        }
    }


    internal sealed class BrushAnimation : AnimationTimeline
    {
        public static readonly DependencyProperty FromProperty = DependencyProperty.Register(nameof(From), typeof(Brush), typeof(BrushAnimation));

        public static readonly DependencyProperty ToProperty = DependencyProperty.Register(nameof(To), typeof(Brush), typeof(BrushAnimation));


        public BrushAnimation()
        {
        }

        public BrushAnimation(Brush toValue, Duration duration)
        {
            To = toValue;
            Duration = duration;
        }
        
        public BrushAnimation(Brush fromValue, Brush toValue, Duration duration)
        {
            From = fromValue;
            To = toValue;
            Duration = duration;
        }


        public override object GetCurrentValue(object defaultOriginValue, object defaultDestinationValue, AnimationClock animationClock)
        {
            return GetCurrentValue(defaultOriginValue as Brush, defaultDestinationValue as Brush, animationClock);
        }

        public object GetCurrentValue(Brush defaultOriginValue, Brush defaultDestinationValue, AnimationClock animationClock)
        {
            if (!animationClock.CurrentProgress.HasValue)
            {
                return Brushes.Transparent;
            }

            defaultOriginValue = From ?? defaultOriginValue;
            defaultDestinationValue = To ?? defaultDestinationValue;

            if (animationClock.CurrentProgress.Value == 0)
            {
                return defaultOriginValue;
            }

            if (animationClock.CurrentProgress.Value == 1)
            {
                return defaultDestinationValue;
            }

            return new VisualBrush(new Border()
            {
                Width = 1,
                Height = 1,
                Background = defaultOriginValue,
                Child = new Border()
                {
                    Background = defaultDestinationValue,
                    Opacity = animationClock.CurrentProgress.Value,
                }
            });
        }


        protected override Freezable CreateInstanceCore() => new BrushAnimation();

        public override Type TargetPropertyType => typeof(Brush);

        public Brush From
        {
            get { return (Brush)GetValue(FromProperty); }
            set { SetValue(FromProperty, value); }
        }

        public Brush To
        {
            get { return (Brush)GetValue(ToProperty); }
            set { SetValue(ToProperty, value); }
        }
    }

    internal sealed class RainbowAnimationSet
    {
        private static readonly string[][] __properties = new string[6][];

        private static readonly Brush[] __brushes = new[] { Brushes.Red, Brushes.Yellow, new SolidColorBrush(Color.FromRgb(0, 0xFF, 0)), Brushes.Cyan, Brushes.Blue, Brushes.Magenta };


        private readonly MainWindow _window;

        private Storyboard _currentStoryboard;

        private int _currentStoryboardLevel;

        private Duration _currentStoryboardDuration;


        static RainbowAnimationSet()
        {
            __properties[0] = new string[] { nameof(MainWindow.Led5Brush) };
            __properties[1] = new string[] { nameof(MainWindow.Led4Brush), nameof(MainWindow.Led6Brush) };
            __properties[2] = new string[] { nameof(MainWindow.Led3Brush), nameof(MainWindow.Led14Brush), nameof(MainWindow.Led7Brush) };
            __properties[3] = new string[] { nameof(MainWindow.Led2Brush), nameof(MainWindow.Led13Brush), nameof(MainWindow.Led8Brush) };
            __properties[4] = new string[] { nameof(MainWindow.Led12Brush), nameof(MainWindow.Led15Brush) };
            __properties[5] = new string[] { nameof(MainWindow.Led1Brush), nameof(MainWindow.Led11Brush), nameof(MainWindow.Led10Brush), nameof(MainWindow.Led9Brush) };
        }

        public RainbowAnimationSet(MainWindow window)
        {
            _window = window;
        }


        private void CurrentStoryboard_Completed(object sender, EventArgs e)
        {
            _currentStoryboardLevel = NextLevel(_currentStoryboardLevel);

            RunNext();
        }


        public void Run()
        {
            _currentStoryboardLevel = 0;
            _currentStoryboardDuration = new Duration(TimeSpan.FromSeconds(1));

            RunNext();
        }

        private void RunNext()
        {
            if (_currentStoryboard != null)
            {
                _currentStoryboard.Completed -= CurrentStoryboard_Completed;

                _currentStoryboard.Children.Clear();

                _currentStoryboard = null;
            }


            _currentStoryboard = new Storyboard();

            _currentStoryboard.Completed += CurrentStoryboard_Completed;


            var level = _currentStoryboardLevel;

            foreach (var brush in __brushes)
            {
                CreateAndSetAnimations(brush, __properties[level]);

                level = PreviousLevel(level);
            }

            _currentStoryboard.Begin();
        }

        private int NextLevel(int level)
        {
            return (level + 1) % 6;
        }

        private int PreviousLevel(int level)
        {
            level -= 1;

            return level < 0 ? 5 : level;
        }

        private void CreateAndSetAnimations(Brush toBrush, string[] propertyNames)
        {
            foreach (var propertyName in propertyNames)
            {
                var animation = new BrushAnimation(toBrush, _currentStoryboardDuration);

                Storyboard.SetTarget(animation, _window);
                Storyboard.SetTargetProperty(animation, new PropertyPath(propertyName));

                _currentStoryboard.Children.Add(animation);
            }
        }
    }
}
